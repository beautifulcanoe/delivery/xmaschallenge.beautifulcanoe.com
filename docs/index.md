# Win a £75 Amazon voucher in the Beautiful Canoe 2021 Christmas Challenge!

![Win a £75 Amazon voucher this Christmas!](./figures/xmas-challenge-hero.png)

Are you an Aston University student in the [Computer Science](https://www.aston.ac.uk/courses/computer-science) department?
Do you want to have fun over the Christmas break _and_ maybe win a great prize?!
Want to have a neat front-end project to show potential employers?
Enter the Beautiful Canoe Christmas challenge, and you might win a £75 Amazon voucher!

All you need to do, is to reproduce this image, as accurately as you can, in pure HTML5 and CSS:

![The target image that you should reproduce in pure CSS and HTML5](./figures/BC-challenge-image.png)

## Your competition entry

Your entry should be:

* Made from pure CSS and HTML5 only, with no JavaSript or frameworks.
* Your logo should be 400px x 400px in size.
* You should use the same colour as the target image and the [Didact Gothic](https://fonts.google.com/specimen/Didact+Gothic) font.

### The winner

The winning CSS will be the entry that is closest to a pixel-perfect reproduction of the target image.
The winner will be chosen by Beautiful Canoe.

### Submitting your entry

Send an email from your Aston student account to [`tech@beautifulcanoe.com`](mailto:tech@beautifulcanoe.com) with the subject **2021 Xmas challenge** and a link to your entry on the web.
To be considered for the prize, you must submit your entry by **23:59 14th January 2022**.
The winner will be announced no later than January 21st 2022.

## FAQ

### I'm on an Apprenticeship course, an MSc course, or a Business Computing course, can I still enter?

Yes, if you are studying for any course in the [Computer Science](https://www.aston.ac.uk/courses/computer-science) department, you can enter.

### What font is used for the Beautiful Canoe initials?

The font is [Didact Gothic](https://fonts.google.com/specimen/Didact+Gothic).

### What is the prize?

The prize will be a £75 Amazon voucher.

### I don't have a website, where can I host my entry?

Try [jsfiddle](https://jsfiddle.net/) or [Codepen](https://codepen.io/) or [GitHub Pages](https://pages.github.com/).

### I don't know CSS very well, how can I learn more?

We recommend these resources:

* [web.dev](https://web.dev/learn/css/)
* [Codeacademy](https://www.codecademy.com/learn/learn-css)
* [CSS-Tricks](https://css-tricks.com/)
* [CSSBattle](https://cssbattle.dev/)
* [freeCodeCamp course on YouTube](https://www.youtube.com/watch?v=1Rs2ND1ryYc)
* [Kevin Powell's YouTube channel](https://www.youtube.com/kepowob/)
* [Traversy Media crash course on YouTube](https://www.youtube.com/watch?v=yfoY53QXEnI)

### Does my entry need to look right on all browsers?

Yes, it should look "right" on at least recent versions of Chrome, Firefox, Safari, and Edge.
You can test your work on sites like [Browser Shots](http://browsershots.org/) or [Browser Stack](https://www.browserstack.com/screenshots).

### Does my entry need to look right on mobile devices?

If your entry looks right on desktop devices, it will probably be fine on mobile devices.
However, for this competition you do not need to test your work specifically for mobile.

### Can I use JavaScript?

Nope, pure CSS and HTML5 only, please.

### Can I use an image (e.g. a PNG or JPEG)?

Nope.

### Can I use a Less, SASS, or another CSS pre-processor?

Seriously, no.

### Can I submit more than one entry?

No, sorry.

### Why is the Christmas tree upside down?

[This](https://xkcd.com/835/) is why.

## Terms and conditions

* This competition is open to current Aston university students only.
* Only one entry per person.
* Entries must submitted by 23:59 14th January 2022.
* The winner will be announced no later than January 21st 2022.
* The decision of the judges is final.
* The winning CSS will be the entry that is closest to a pixel-perfect reproduction of the target image.
* If more than one entry matches the target image, the entry with the simplest and smallest code will win.
